describe('fazer login', () => {
    it('login ', () => {
        cy.clearLocalStorage()
        cy.viewport(1440, 900)
        cy.visit('https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap')


    })
    it('Mudar o valor da combo Select version para Bootstrap V4 Theme', () => {
        cy.get('#switch-version-select').select('Bootstrap V4 Theme')
        cy.get('.floatL.t5 > .btn').click()
    })

    it('Should fill correctly the Identificação form', () => {
        cy.get('input[id=field-customerName]').type('teste sicredi')
        cy.get('#field-contactLastName').type('teste ')
        cy.get('#field-contactFirstName').type('petala')
        cy.get('#field-phone').type('519999-9999')
        cy.get('#field-addressLine1').type('Av Assis Brasil, 3970')
        cy.get('#field-addressLine2').type('torre d')
        cy.get('#field-city').type('porto alegre')
        cy.get('#field-state').type('rs')
        cy.get('#field-postalCode').type('91000-000')
        cy.get('#field-country').type('brasil')
        cy.get('#field-salesRepEmployeeNumber').type('fixter')
        cy.get('#field-creditLimit').type('200')
        cy.get('#form-button-save').click()
    })

    it('editar cliente,savar a lista e fechar o browser', () => {
        cy.get('.go-to-edit-form').click()
        cy.get('#form-button-save').click()
    })
})